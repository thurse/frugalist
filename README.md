# Frugalist

Keeps a list of things you want to buy to postpone your purchase decision, helping you to control your consumerism.

Everything you feel a buying need for you put on the list and try to not think about
it too much. If the item expires you'll recide if you still want to buy it.

## TODO

- Add an *edit* functionality
- bash scripting is a mess - move to python

## Prerequisites

It's a bash script which heavily relies on [gawk](https://www.gnu.org/software/gawk/).

## Setup

Download, adjust the `database` variable to a writeable path and install it to a directory
which is within your user's *$PATH*, e.g. with `install ./frugalist ~/bin/`.

#### Bash completions
Bash completions are provided via [frugalist.completions](./frugalist.completions).
Source this file or add the contents to your completion config to make the usage of
frugalist more pleasing.

#### systemd timer
`frugalist check` or rather `frugalist notify` are meant to be invoked in a certain
interval to keep you updated about your expired items. You can use a cron job for
that, however I like systemd's timers more. In [systemd/](./systemd/) you'll
find simple *.service* and *.timer* files, to put in your systemds user directory
(usually *$HOME/.config/systemd/user*). Adjust the path to frugalist in 
[frugalist.service](./systemd/frugalist.service) and enable the timer with
`systemctl --user enable frugalist.timer`.

## Usage

Run `frugalist help` for details:

```
Usage: frugalist [action] [argument]

Keeps a list of things you want to buy to postpone your purchase decision,
helping you to control your consumerism.

actions:
    [no action]|list
                lists all available items, their description and expiry time in days

    add         adds a new item to the list
                [no argument]                     interactive addition
                [name] [expiration] [description] adds an item with a common name,
                                                  an optional expiration time in days
                                                  (default: 30) and an optional
                                                  description

    check       lists all items (over-)due to make your buying decision

    delay       postpones the expiration date of an item
                [no argument]                     interactive delay
                [item number]                     delays the expiry of an item
                                                  by the default of 7 days
                [item number] [delay]             delays an item by [delay] days

    delete      deletes an item from the list
                [no argument]                     interactive deletion
                [item number]                     deletes an item by number
                due                               deletes all items which are due, i.e.
                                                  having remaining days <= 0
                overdue                           deletes all items which are overdue
                                                  (default: 3 days)

    dump        outputs an unformatted list for easy post processing; the field
                delimiters are tabulators

    help        displays this help

    notify      notifies other programs about items which are (over-)due via the
                notify function (wrapper for check)

```
